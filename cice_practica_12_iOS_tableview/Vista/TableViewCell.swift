//
//  TableViewCell.swift
//  cice_practica_12_iOS_tableview
//
//  Created by Diego Angel Fernandez Garcia on 21/2/19.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var nombreProducto: UILabel!
    @IBOutlet weak var imagenProducto: UIImageView!{
        didSet{
            //imagenProducto.layer.cornerRadius = 29
           // imagenProducto.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
