//
//  DetailViewController.swift
//  cice_practica_12_iOS_tableview
//
//  Created by Diego Angel Fernandez Garcia on 21/2/19.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var imagen: UIImageView!
    var nombreImagen = "imagen"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagen.image = UIImage(named: self.nombreImagen)
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
