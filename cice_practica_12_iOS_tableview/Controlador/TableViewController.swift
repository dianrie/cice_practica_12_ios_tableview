//
//  TableViewController.swift
//  cice_practica_12_iOS_tableview
//
//  Created by Diego Angel Fernandez Garcia on 19/2/19.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    var listaCompra = ["Café","Pan","Leche","Harina","Platanos","Nueces","Pescado","Platanos","Zanaoria","Miel"]
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Lista Compra"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listaCompra.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell

        // Configure the cell...
        cell.nombreProducto.text = listaCompra[indexPath.row]
        cell.imagenProducto.image = UIImage(named:  listaCompra[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //Creamos nuestras acciones
        //Borrar row
        let borrarCelda = UIContextualAction(style: .destructive, title: "Borrar") { (action, sorceView, completionHandler) in
            self.listaCompra.remove(at: indexPath.row)
            
            
            //Eliminamos la celda
            self.tableView.deleteRows(at: [indexPath], with:.fade)
            
            //Recargamos la tabla
            self.tableView.reloadData()
            
            //Avisamos al completionHandler de que hemos terminado
            completionHandler(true)
        }
        
        //Creamos un UISwipeActionsConfiguration
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [borrarCelda])
        return swipeConfiguration
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "showDetail"){
                let controladorDestino = segue.destination as! DetailViewController
                if let indexPath = tableView.indexPathForSelectedRow{
                    controladorDestino.title = listaCompra[indexPath.row]
                    controladorDestino.nombreImagen =  listaCompra[indexPath.row]
                }
           }
    }
    
    
    

}
